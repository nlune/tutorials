pkg load statistics
% A4 E2 Part 2 Gaussian Processes

% xs = [-0.5, 0.2, 0.3, -0.1]';
% ts = [0.5, -1, 3, -2.5]';
xs = linspace(-5,5,50)'; % try out predictions with different data
ts =  sin(xs);
th = [1,1,1,1];

function dist = kernel(x, xi, theta)
  normv = norm(x-xi);
  dist = theta(1) * exp(-(theta(2)/2) * normv.^2) + theta(3) + theta(4)*x'*xi;
endfunction


kernel_matrix = zeros(length(xs),length(xs));

for i = 1:length(xs)
  for j = 1:length(xs)
    kernel_matrix(i,j) = kernel(xs(i), xs(j), th);
  endfor
endfor

noise = eye(length(xs),length(xs));
C = kernel_matrix + 0.5*noise;

new_x = linspace(-5,5,70);
k = zeros(length(xs),length(new_x));
for i = 1:length(xs)
  for j= 1:length(new_x)
  k(i,j) = kernel(xs(i), new_x(j), th);
endfor
endfor

c = kernel(new_x, new_x, th);


mx = k'*inv(C)*ts;
varx = c - k'*inv(C)*k;

hold on
plot(xs, ts, 'o', new_x, mx, 'r--')
titext = sprintf('Predicted sine with uncertainty') % at %5.3f and variance %5.3f', mx, varx);
title(titext)
xlabel("x")
ylabel("t")
legend('50 training points','GP fit')
axis([-6,6,-1.3, 1.3])
hold off
