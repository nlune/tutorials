pkg load statistics

% =======================Gaussian Processes, 1D data=============================
datapts = linspace(-1,1,101);

% kernel function
function dist = kernel(x, xi, theta)
  normv = norm(x-xi);
  dist = theta(1) .* exp(-(theta(2)/2) .* normv.^2) .+ theta(3) .+ theta(4).*x'*xi;
endfunction

% create gram matrix kernel
% dim K = 101x101
t = [1,4,0,0; 9,4,0,0; 1, 64, 0, 0; 1, 0.25, 0, 0; 1, 4, 10, 0; 1, 4, 0, 5];
thetas = cell(1,6);
for i=1:6
  thetas(i) = t(i, :);
endfor

% calculate kernel matrix for each set of theta kernel parameters
kernel_matrices = cell(1,6);
for k= 1:6

for i = 1:length(datapts)
  for j = 1:length(datapts)
    kernel_matrix(i,j) = kernel(datapts(i), datapts(j), thetas{k});
  endfor
endfor

kernel_matrices(k) = kernel_matrix;

% Even when K (kernel cov. matrix) is positive definite, some of its eigenvalues may be too small to accu-
% rately compute. You can alleviate this
% issue by adding a small diagonal term to K.
% eps = 1e-5;

% k_matrix = kernel_matrices(k) +  eps*eye(length(datapts), length(datapts));
% try chol(k_matrix);
%     disp('Successful Cholesky decomposition: Matrix is symmetric positive definite.')
% catch ME
%     disp('Matrix is not symmetric positive definite')
% end

% if all(eig(k_matrix > 0);
%   disp("All positive eigenvalues.")
%   eig(_matrix(1:10)
% endif
endfor

% vector zero mean
means = zeros(1, length(datapts));
ys = cell(1,5);

% sample output ys from multivar. gaussian with zero mean and kernel covariance matrix
for i=1:5
  ys(i) = mvnrnd(means, kernel_matrices{6});
endfor


% plot(datapts, ys{1}, datapts, ys{2}, datapts, ys{3}, datapts, ys{4}, datapts, ys{5})
% title("5 functions sampled from GP prior using theta = (1, 4, 0, 5)")
% xlabel("x")
% ylabel("y(x)")


% ===============================GPs with 2D data=================================
% input is now x and y axis, tuple for input. 
gridsize = 50
xs = linspace(-1,1,gridsize);
ys = linspace(-1,1,gridsize);
dgrid = cell(gridsize,gridsize);

% fill grid with xy inputs
for i=1:length(xs)
  for j = 1:length(ys)
    dgrid(i,j) = [xs(i); ys(j)];
  endfor
endfor

flat_dgrid = reshape(dgrid, 1, gridsize^2);

% different parameters for kernel function
th = [1, 1, 1, 1;
      1,10,1,1;
      1,1,1,10];


% dgrid is our grid of 2D datapoints
% flattened for kernel matrix calculation then reshaped after sampling
% printf('%s',"observations matrix \n")
% for i = 1:5
%   for j = 1:5
%     fprintf('%5.3f %5.3f \n',dgrid{i,j})
%   endfor
% endfor


% calculate new kernel matrix for 2D data
% in the end with the flattened grid we get 441x441 covariance matrix
% K = zeros(441,441); % kernel gram matrix
% Ks = cell(1,3); % store kernel matrices for different thetas
% for k= 1:3
%   for i = 1:length(flat_dgrid)
%     for j = 1:length(flat_dgrid)
%       K(i,j) = kernel(flat_dgrid{i}, flat_dgrid{j}, th(k, :));
%     endfor
%   endfor
%   Ks(k) = K;
% endfor

% % drawing 4 sample functions for each theta -- select which (1-3) by changing Ks{1}
% samples = cell(1,4);
% means = zeros(1, gridsize^2);
% for i=1:4
%   samples(i) = mvnrnd(means, Ks{2});
% endfor


% zs = reshape(samples{1}, gridsize,gridsize);

surf(xs,ys,zs);
title('Sampled function from GP prior using \theta = (1,10, 1,1)')
% xlabel("x1")
% ylabel("x2")
% zlabel("y(X)")
